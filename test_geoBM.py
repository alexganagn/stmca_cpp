import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import scipy.special as spspc
from matplotlib.pyplot import figure
import csv
import os


#matplotlib.use('pgf')
folder="output/"
extension=".pgf"


def import_data(namefile):
    # List to store the values
    out = []
    # Open the file and read values line by line
    with open(namefile, 'r') as csvfile:
        csvreader = csv.reader(csvfile, delimiter=';')
        for line in csvreader:
            # Strip any leading or trailing whitespace
            val = [float(item) for item in line]
            # Append the value to the list
            out.append(val[0])
    return out


def lognormal_ptk(t,x,y,mu,sigma):
	V = t * sigma ** 2
	E = - ((np.log(y) - mu + t * (sigma ** 2)/2 - np.log(x)) ** 2) / (2 * V)
	N = y * np.sqrt(2 * np.pi * V)
	return np.exp(E)/N;


terminal_values=import_data(folder+"TV_geoBM.csv")

x0 = 10
t = 1
sigma = 0.25
mu = 0
xx = np.linspace(0, 70, num=1000, endpoint=True, retstep=False, dtype=None, axis=0)
yy = [lognormal_ptk(t,x0,x,mu,sigma) for x in xx]


if True:
	namefile="hist_geoBM_1"
	plt.clf()
	plt.figure(figsize=(8,6))
	plt.hist(terminal_values, bins=40, edgecolor='white', color = "mediumturquoise", alpha=0.8, density=True)
	plt.plot(xx,yy, color="blue",linewidth=1.5) # https://sites.google.com/view/paztronomer/blog/basic/python-colors
	plt.savefig(folder+namefile+".png", bbox_inches='tight')


terminal_values=import_data(folder+"TV_geoBM_2.csv")

x0 = 10
t = 10
sigma = 0.25
mu = 0
xx = np.linspace(0, 70, num=1000, endpoint=True, retstep=False, dtype=None, axis=0)
yy = [lognormal_ptk(t,x0,x,mu,sigma) for x in xx]


if True:
	namefile="hist_geoBM_2"
	plt.clf()
	plt.figure(figsize=(8,6))
	plt.hist(terminal_values, bins=74, edgecolor='white', color = "mediumturquoise", alpha=0.8, density=True)
	plt.plot(xx,yy, color="blue",linewidth=1.5) # https://sites.google.com/view/paztronomer/blog/basic/python-colors
	plt.savefig(folder+namefile+".png", bbox_inches='tight')

