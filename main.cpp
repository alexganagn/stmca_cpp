#include <iostream>
#include <random>
#include <cmath>
#include <fstream>
#include <gsl/gsl_sf_dawson.h>

#include "src/grid/grid.h"
#include "src/diffusion/diffusion.h"
#include "src/diffusion_tabular/diffusion_tabular.h"
#include "src/trajectory_simulation/trajsim_adv.h"

#include "src/diffusion/sticky.h"
#include "src/diffusion/skew.h"
#include "src/diffusion/ou_process.h"
#include "src/diffusion/cir_process.h"
#include "src/diffusion/skew_bessel.h"
#include "src/diffusion/geo_sticky.h"

using namespace std;

int main() {
    // Define diffusion parameters
    double sigma0 = 0.25;
    double mu0 = 0.0;
    double zeta0 = 10.0;

    // Create instances of the Geo_sticky diffusion process with different parameters
    double rho0 = 0.0;
    Geo_sticky diffusion(mu0, sigma0, rho0, zeta0); 

    // Define domain and grid parameters
    double l_domain = 0;
    double r_domain = 150;
    double grid_thinness = 0.03;

    // GRID COMPUTATION
    grid grid0(l_domain, r_domain, grid_thinness); // Create a grid object
    grid0.geo_BM(zeta0, 1); // Fill the grid using the geometric Brownian motion process
    grid0.export_grid_to_file("output/grid.csv"); // Export the grid to a file

    // Create instances of Diffusion_tabular using the grid and diffusion processes
    Diffusion_tabular tabbed_diffusion(grid0, &diffusion);
    tabbed_diffusion.fill_arrays(); // Compute transition probabilities and times
    tabbed_diffusion.export_arrays("output/tabbed.csv"); // export the results to a file

    // Create instances of the Geo_sticky diffusion process with different parameters
    rho0 = 2.0;
    Geo_sticky diffusion2(mu0, sigma0, rho0, zeta0);

    Diffusion_tabular tabbed_diffusion2(grid0, &diffusion2);
    tabbed_diffusion2.fill_arrays(); // Compute transition probabilities and times
    tabbed_diffusion2.export_arrays("output/tabbed.csv"); // export the results to a file


    // Define parameters for single trajectory simulation
    double T = 1.0; // Time horizon
    double x0 = 10.0; // Initial position

    // SIMULATIONS
    double N_MC=10000; // number of samples
    Trajsim_adv traj_sim(T, x0, tabbed_diffusion, tabbed_diffusion2); // Create a Trajsim_adv object for simulation
    traj_sim.single_traj_export_full_cross("output/sample_path_cross_1.csv", "output/sample_path_cross_2.csv"); // Perform one paired trajectory simulation and export the results to files
    traj_sim.simulate_terminal_values(N_MC); // Simulate <N_MC> trajectories and store terminal values
    traj_sim.export_TV(N_MC,"output/TV_geoBM.csv"); // Export terminal values to file

    // SIMULATIONS
    traj_sim.reset_time(10.0);
    traj_sim.simulate_terminal_values(N_MC); // Simulate <N_MC> trajectories and store terminal values
    traj_sim.export_TV(N_MC,"output/TV_geoBM_2.csv"); // Export terminal values to file


    return 0;
}
