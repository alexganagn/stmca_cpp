import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import scipy.special as spspc
from matplotlib.pyplot import figure
import csv
import os
matplotlib.use('pgf')
folder="output/"
extension=".pgf"


# Some plots
with open(folder+'sample_path_cross_1.csv', 'r') as f:
    times = []
    positions = []
    for line in f:
        # Split the line using semicolons and convert to float
        values = line.strip().split(';')
        time = float(values[0])
        position = float(values[1])
        times.append(time)
        positions.append(position)

with open(folder+'sample_path_cross_2.csv', 'r') as f:
    times2 = []
    positions2 = []
    for line in f:
        # Split the line using semicolons and convert to float
        values = line.strip().split(';')
        time = float(values[0])
        position = float(values[1])
        times2.append(time)
        positions2.append(position)


if True:
	namefile=folder+"sample_pathX"
	plt.clf()
	plt.figure(figsize=(18,6))
	plt.step(times2,positions2, label='TRUE curve',linestyle='solid',color='lightsteelblue')
	plt.step(times,positions, label='TRUE curve',color='blue')
	plt.savefig(namefile+'.png', bbox_inches='tight')


