# Title: STMCA cpp
# Description: C++ implementation of the STMCA algorithm for the simulation of one-dimensional diffusions
# Author: Alexis Anagnostakis (alexis.anagnostakis@yandex.com)

## TABLE OF CONTENTS

- [Overview](#overview)
- [Prerequisites](#prerequisites)
- [Getting Started](#getting-started)
- [Usage](#usage)
- [File Structure](#file-structure)
- [Contributing](#contributing)
- [License](#license)
- [Acknowledgments](#acknowledgments)

## OVERVIEW

This project implements the STMCA algorithm for the simulation of one-dimensional diffusions (see Anagnostakis, Alexis; Lejay, Antoine; Villemonais, Denis, [General diffusion processes as limit of time-space Markov chains.] Ann. Appl. Probab. 33, No. 5, 3620-3651 (2023)).

It contains classes that:
1. compute the quasi-tuned grid for several diffusions (standard Brownian motion, sticky Brownian motion, Cox-Ingersol-Ross process, Ornstein-Uhlenbeck process, geometric Brownian motion)
2. compute the transition probabilities and conditional expected transition times of the process on a grid
3. given 1,2, simulates the STMCA of the process
4. export simulations, either one single path or terminal values of N simulations

## PREQUISITES

- C++ compiler
- cmake
- GSL library (GNU Scientific Library)

## GETTING STARTED

To get started with the project, follow these steps:

```bash
# Clone the repository
git clone https://github.com/your-username/diffusion-simulation.git
cd diffusion-simulation
# Build the project (provide any specific build instructions if necessary)
sh clean_cache.sh
cmake .
make
./a.out


## USAGE
The user may implement a new diffusion (for example custom_diffusion). To do that he must
1. create the respective custom_diffusion.cpp and custom_diffusion.h that implement the virtual class diffusion in src/diffusion/
2. in the file give the closed form expressions of the v0: transition probabilities and v1:transition times or if not available their derivatives dv0,dv1 (the latter case will numerically compute v0,v1 based on dv0,dv1)
3. add the custom_diffusion.cpp, custom_diffusion.h files to the file CMakeLists.txt
4. implement the grid tuning method for this diffusion in grid.cpp and grid.h


## FILE STRUCTURE
diffusion/: Contains classes for general diffusion processes
	diffusion.h: Header file for the Diffusion class
	diffusion.cpp: Implementation of the Diffusion class
diffusion_tabular/: Classes for tabular diffusion processes
	diffusion_tabular.h: Header file for the Diffusion_tabular class
	diffusion_tabular.cpp: Implementation of the Diffusion_tabular class
helper_functions/: General utility functions
	universal_functions.h: Header file for the Universal Functions class
	universal_functions.cpp: Implementation of the Universal Functions class
grid/: Grid-related classes
	grid.h: Header file for the Grid class
	grid.cpp: Implementation of the Grid class
trajectories/: Classes for simulating trajectories
	trajsim_adv.h: Header file for the Trajsim_adv class
	trajsim_adv.cpp: Implementation of the Trajsim_adv class

## LICENSE

MIT License (see LICENSE.txt)
