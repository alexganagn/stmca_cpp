#!/bin/sh

rm -rf CMakeFiles/*
rm -f cmake_install.cmake
rm -f CMakeCache.txt
rm -f Makefile
rm -f approximation_scheme

rm -f *.jpg
rm -f *.csv
rm -f *.gz
rm -f *.log
