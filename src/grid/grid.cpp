#include "grid.h"

using namespace std;

/*
 * CONSTRUCTORS
 */

double quadratic_decreasing(double x, double k)
{
    return 1 / (abs(pow(x, k)) + 1);
}

grid::grid()
{
    // Default constructor
    // Initialize grid parameters with default values
    left_limit = -10;
    right_limit = 10;
    median_point = (right_limit - left_limit) / 2;
    h = 0.01;
    N = int(15 * (right_limit - left_limit) / h) - 1;
    p_arr = (double *)malloc(sizeof(double) * N);
    fill_grid_unif();
}

grid::grid(double left_limit0, double right_limit0, double size_criteria0)
{
    // Constructor with parameters
    // Initialize grid parameters with user-defined values
    left_limit = left_limit0;
    right_limit = right_limit0;
    median_point = (right_limit - left_limit) / 2;
    h = size_criteria0;
    N = int(5 * (right_limit - left_limit) / h) + 2;
    p_arr = (double *)malloc(sizeof(double) * N);
}


/*
 * PUBLIC FUNCTIONS
 */


int grid::export_grid_to_file(string filename)
// EXPORT GRID TO FILE
{
    // Open the file for writing
    ofstream myfile;
    myfile.open(filename, ofstream::out | ofstream::trunc);

    // Check if the file is opened successfully
    if (myfile.is_open())
    {
        // Write grid elements to the file
        for (int i = 0; i < N; i++)
        {
            myfile << *(p_arr + i) << endl;

            // Display progress using an activity indicator
            universal_functions::activity_indicator(i, N, "EXPORT STATUS: ");
        }

        // Display completion status
        universal_functions::completed_activity("EXPORT STATUS: ");

        // Close the file
        myfile.close();

        // Print a success message
        cout << "FILE CREATED: " << filename << endl << endl;
    }
    else
    {
        // Print an error message if unable to open the file
        cout << "Unable to open file" << endl;
    }

    return 0;
}


int grid::print_matrix()
{
    // Print the grid matrix
    for (int i = 0; i < N; i++)
    {
        cout << i << " " << *(p_arr + i) << endl;
    }
    return 0;
}


double grid::get_matrix_cell(int n)
{
    // Return the value at the specified index 'n' in the grid
    return *(p_arr + n);
}


int grid::set_matrix_cell(int n, double x)
{
    // Set the 'n'-th element of the grid to a specific value
    p_arr[n]=x;

    return 0;
}


// GRID TUNING FUNCTIONS

int grid::fill_grid_unif()
// a uniform grid
{
    double *arr = p_arr;

    int i = 0;
    while (i < N)
    {
        arr[i] = left_limit + i * h;
        i++;
    }

    return 0;
}



int grid::fill_grid_sticky(double sticky_point_location)
// Fill the grid quasi-adapted to the sticky Brownian motion
{
    double *arr = p_arr;
    arr[0] = left_limit;
    double arr1[N + 2];
    double arr2[N + 2];

    arr1[0] = sticky_point_location;
    arr1[1] = sticky_point_location + h * h / 4;
    int i = 2;

    // Fill the right side of the grid
    while (arr1[i - 1] + h <= right_limit)
    {
        arr1[i] = arr1[i - 1] + h;
        i++;
    }

    // Check if the last element exceeds the right limit
    if (arr1[i - 1] + h > right_limit)
    {
        arr1[i] = right_limit;
        i++;
    }

    int j = 1;
    arr2[0] = sticky_point_location - h * h / 4;

    // Fill the left side of the grid
    while (arr2[j - 1] - h >= left_limit)
    {
        arr2[j] = arr2[j - 1] - h;
        j++;
    }

    // Check if the last element is less than the left limit
    if (arr2[j - 1] - h < left_limit)
    {
        arr2[j] = left_limit;
        j++;
    }

    int N1 = i;
    int N2 = j;

    // Copy elements from arr2 and arr1 to the grid
    for (j = 0; j <= N2; j++)
    {
        arr[N2 - j - 1] = arr2[j];
    }

    for (i = 0; i < N1; i++)
    {
        arr[N2 + i] = arr1[i];
    }

    return 0;
}


int grid::fill_grid_cir(double sticky_point_location, double inflation)
// Fill the grid quasi-adapted to the CIR process
{
    double *arr = p_arr;
    double arr1[N];
    double dh;
    double candid1;
    double weight = 1;
    int N1;

    // Initialize the grid with the right limit
    arr1[0] = right_limit;
    int i = 0;

    // Fill the grid until it reaches the left limit
    while (arr1[i] - h * h > left_limit)
    {
        if (arr1[i] < 3)
        {
            weight = (0.3 + 1) / 2.0;
            weight = 1.5;
            weight = 1.0;
        }

        // Calculate the next grid point using a quadratic function
        candid1 = fmax(arr1[i] - weight * h, arr1[i] * exp(-2 * weight * h));
        arr1[++i] = fmin(candid1, arr1[i] - weight * h * h);
    }

    N1 = i;

    // Copy elements from arr1 to the grid
    for (int j = 0; j <= N1; j++)
    {
        arr[j] = arr1[N1 - j];
    }

    N = i;
    return 0;
}


int grid::geo_BM(double zeta0, double inflation)
// Fill the grid quasi-adapted to a geometric Brownian motion
{
    double *arr = p_arr;
    double arr1[N];
    double dh;
    double x_tmp;
    double weight = 1;
    int N1;
    double sigma = 1.0;

    // Initialize the grid with the right limit
    arr1[0] = right_limit;
    int i = 0;

    // Fill the grid until it reaches the left limit
    while (arr1[i] - h * h > left_limit)
    {
        x_tmp = 1 / (pow(sigma, 2) * h / 2 + 1 / arr1[i]);
        x_tmp = fmax(x_tmp, arr1[i] - h);
        arr1[++i] = fmin(x_tmp, arr1[i] - h * h);
    }

    N1 = i;

    // Copy elements from arr1 to the grid
    for (int j = 0; j <= N1; j++)
    {
        arr[j] = arr1[N1 - j];
    }

    N = N1;
    return 0;
}


/*
 * END OF FILE
 */
 
 
 
 
