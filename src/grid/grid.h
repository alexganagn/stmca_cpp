#ifndef GRID_H
#define GRID_H

#include <iostream>
#include <fstream>
#include "../helper_functions/universal_functions.h"

using namespace std;

// Class definition for the grid
class grid {
public:
    // Grid parameters
    double h;               // Step size
    double left_limit;      // Left limit of the grid
    double right_limit;     // Right limit of the grid
    double median_point;    // Median point of the grid
    int N;                  // Number of grid points

    // Pointer to an array representing the grid
    double *p_arr;

    // Constructors
    grid(double left_limit0, double right_limit0, double size_criteria0);
    grid();

    // Methods for computing tuned grids to different processes
    int fill_grid_unif();   // Uniform grid
    int fill_grid_sticky(double sticky_point_location);  // Sticky Brownian motion
    int fill_grid_cir(double sticky_point_location, double inflation);  // CIR process
    int geo_BM(double zeta, double inflation); // geometric Brownian motion

    // Method for tuning the grid
    int grid_tuning();

    // Method for exporting the grid to a file
    int export_grid_to_file(string filename);

    // Method for printing the grid matrix
    int print_matrix();

    // Method for accessing a specific cell in the grid
    double get_matrix_cell(int n);
    int set_matrix_cell(int n, double x);
};

#endif // GRID_H


