// diffusion_tabular.cpp

#include "diffusion_tabular.h"

using namespace std;

/* ************ Constructors ************ */


Diffusion_tabular::Diffusion_tabular() {
    // Default constructor
}


Diffusion_tabular::Diffusion_tabular(grid grid00, Diffusion *pdiff00) {
    // Constructor with parameters
    grid0 = grid00;
    pdiff0 = pdiff00;

    pplus = (double *) malloc(sizeof(double)*grid0.N);
    Tplus = (double *) malloc(sizeof(double)*grid0.N);
    Tminus = (double *) malloc(sizeof(double)*grid0.N);
}


Diffusion_tabular::~Diffusion_tabular() {
    // Destructor
}


int Diffusion_tabular::fill_arrays() {
    // Function to fill arrays based on the type of diffusion process
    if ((pdiff0 -> is_numerical_v0) == 1) {
        if ((pdiff0 -> is_numerical_v1) == 1) {
            cout << "Diffusion process of TYPE 1: closed formulas for v0 and v1" << endl;
            fill_arrays_type1();
        } else {
            cout << "Diffusion process of TYPE 2: closed formulas for v0" << endl;
            fill_arrays_type2();
        }
    } else {
        cout << "Diffusion process of TYPE 3: with no closed formulas" << endl;
        fill_arrays_type3();
    }
    return 0;
}


// Fill arrays for diffusion process type 1
int Diffusion_tabular::fill_arrays_type1()
{
    double a, x, b;
    for (int i = 1; i < grid0.N - 1; i++)
    {
        // Extract neighboring values from the grid
        a = grid0.get_matrix_cell(i - 1);
        x = grid0.get_matrix_cell(i);
        b = grid0.get_matrix_cell(i + 1);

        // Compute probabilities and transition times based on closed formulas for v0 and v1
        pplus[i] = pdiff0->v0(a, x, b);
        Tplus[i] = pdiff0->v1_plus(a, x, b) / pplus[i];
        Tminus[i] = pdiff0->v1_minus(a, x, b) / (1 - pplus[i]);

        // Display progress indicator
        universal_functions::activity_indicator(i, grid0.N - 1, "COMPUTING TRANSITION PROBABILITIES & TIMES: ");
    }
    // Display completion message
    universal_functions::completed_activity("COMPUTING TRANSITION PROBABILITIES & TIMES: ");
    cout << endl;
    return 0;
}


// Fill arrays for diffusion process type 2
int Diffusion_tabular::fill_arrays_type2()
{
    double a, x, b;
    for (int i = 1; i < grid0.N - 1; i++)
    {
        // Extract neighboring values from the grid
        a = grid0.get_matrix_cell(i - 1);
        x = grid0.get_matrix_cell(i);
        b = grid0.get_matrix_cell(i + 1);

        // Compute probabilities and transition times using numerical methods for v1
        pplus[i] = pdiff0->v0(a, x, b);
        Tplus[i] = pdiff0->v1_plus_numerical(a, x, b, 250) / pplus[i];
        Tminus[i] = pdiff0->v1_minus_numerical(a, x, b, 250) / (1 - pplus[i]);

        // Display progress indicator
        universal_functions::activity_indicator(i, grid0.N - 1, "COMPUTING TRANSITION PROBABILITIES & TIMES: ");
    }
    // Display completion message
    universal_functions::completed_activity("COMPUTING TRANSITION PROBABILITIES & TIMES: ");
    cout << endl;
    return 0;
}


// Fill arrays for diffusion process type 3
int Diffusion_tabular::fill_arrays_type3()
{
    double a, x, b;
    for (int i = 1; i < grid0.N - 1; i++)
    {
        // Extract neighboring values from the grid
        a = grid0.get_matrix_cell(i - 1);
        x = grid0.get_matrix_cell(i);
        b = grid0.get_matrix_cell(i + 1);

        // Fill a lookup table for v0 and then compute probabilities and transition times using numerical methods for v1
        pdiff0->fill_v0_tab(a, b);
        pplus[i] = pdiff0->v0(a, x, b);
        Tplus[i] = pdiff0->v1_plus_numerical(a, x, b, 250) / pplus[i];
        Tminus[i] = pdiff0->v1_minus_numerical(a, x, b, 250) / (1 - pplus[i]);

        // Display progress indicator
        universal_functions::activity_indicator(i, grid0.N - 1, "COMPUTING TRANSITION PROBABILITIES & TIMES: ");
    }
    // Display completion message
    universal_functions::completed_activity("COMPUTING TRANSITION PROBABILITIES & TIMES: ");
    cout << endl;
    return 0;
}


// Export arrays to a file
int Diffusion_tabular::export_arrays(string filename)
{
    ofstream myfile;
    myfile.open(filename, ofstream::out | ofstream::trunc);
    if (myfile.is_open())
    {
        // Write header to the file
        myfile << "cell center" << " & " << "cell size ratio " << " & " << "P.T+" << " & " << "E.T.T+" << " & " << "E.T.T-" << endl;

        // Write data to the file
        for (int i = 1; i < grid0.N - 1; i++)
        {
            myfile << grid0.get_matrix_cell(i) << " & " << (grid0.get_matrix_cell(i + 1) - grid0.get_matrix_cell(i)) / (grid0.get_matrix_cell(i) - grid0.get_matrix_cell(i - 1)) << " & " << pplus[i] << " & " << Tplus[i] << " & " << Tminus[i] << endl;

            // Display progress indicator
            universal_functions::activity_indicator(i, grid0.N - 1, "EXPORT STATUS: ");
        }

        // Display completion message
        universal_functions::completed_activity("EXPORT STATUS: ");
        myfile.close();
        cout << "FILE CREATED: " << filename << endl << endl;
    }
    else
    {
        cout << "Unable to open file" << endl;
    }

    return 0;
}


