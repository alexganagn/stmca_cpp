#ifndef DIFFUSION_TABULAR_H
#define DIFFUSION_TABULAR_H

#include "../grid/grid.h"
#include "../diffusion/diffusion.h"
#include "../helper_functions/universal_functions.h"
#include <math.h>
#include <iostream>
#include <fstream>
#include <cmath>

class Diffusion_tabular
{
protected:
    // Member variables

public:
    // Constructors and Destructor
    Diffusion_tabular();  // Default constructor
    Diffusion_tabular(grid grid00, Diffusion *pdiff00);  // Constructor with parameters
    ~Diffusion_tabular();  // Destructor

    // Member variables
    Diffusion *pdiff0;  // Pointer to a Diffusion object
    grid grid0;         // Grid object

    double *pplus;   // Pointer to an array for storing pplus values
    double *Tplus;   // Pointer to an array for storing Tplus values
    double *Tminus;  // Pointer to an array for storing Tminus values

    // Virtual functions for filling arrays
    virtual int fill_arrays();       // General function for filling arrays
    virtual int fill_arrays_type1();  // Function for filling arrays of type 1, closed formulas for transition probabilities and times
    virtual int fill_arrays_type2();  // Function for filling arrays of type 2, closed formula for transition probabilities
    virtual int fill_arrays_type3();  // Function for filling arrays of type 3, no closed formula

    // Function for exporting arrays to a file
    int export_arrays(string filename);
};

#endif // DIFFUSION_TABULAR_H

