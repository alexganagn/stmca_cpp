#include "diffusion.h"
#include "../helper_functions/universal_functions.h"
#ifndef OU_PROCESS_H
#define OU_PROCESS_H


class Ou_process : public Diffusion
{
private:
    double alpha,mu,sigma,rho;
    double scale(double x, double a, double b);
    double green(double a,double b,double x,double y);
public:

    Ou_process();
    Ou_process(double alpha0, double mu0, double sigma0, double rho0);
    ~Ou_process();

    double v0(double x, double a, double b);
    double v1_plus(double x, double a, double b);
    double v1_minus(double x, double a, double b);
    double dv0(double a, double z, double b);
    double dv1_plus(double a, double x, double y, double b);
    double dv1_minus(double a, double x, double y, double b);
    double v1_plus_numerical(double a, double x, double b, int N);
    double v1_minus_numerical(double a, double x, double b, int N);
};

#endif // OU_PROCESS_H
