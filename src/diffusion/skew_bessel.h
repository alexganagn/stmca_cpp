#include "diffusion.h"
#ifndef SKEW_BESSEL_H
#define SKEW_BESSEL_H


class Skew_bessel : public Diffusion
{
private:
    double delta,beta,rho,nu;
    double scale(double x, double a, double b);
    double green(double a,double b,double x,double y);

public:
    Skew_bessel();
    Skew_bessel(double delta0, double beta0, double rho0);
    ~Skew_bessel();

    double v0(double x, double a, double b);
    double v1_plus(double x, double a, double b);
    double v1_minus(double x, double a, double b);
    double dv0(double a, double z, double b);
    double dv1_plus(double a, double x, double y, double b);
    double dv1_minus(double a, double x, double y, double b);
    double v1_plus_numerical(double a, double x, double b, int N);
    double v1_minus_numerical(double a, double x, double b, int N);
};

#endif // SKEW_BESSEL_H 
