#include "sticky.h"
#include <math.h>
#include <iostream>

using namespace std;


/* ************ Constructors ************ */

Sticky::Sticky()
{
    is_numerical_v0 = 1;
    is_numerical_v1 = 1;
    rho = 1;
}

Sticky::Sticky(double x)
{
    is_numerical_v0 = 1;
    is_numerical_v1 = 1;
    rho = x;
}

Sticky::~Sticky()
{

}


/* ************ Private functions ************ */

double Sticky::scale(double x, double a, double b)
{
    return x;
}

double Sticky::green(double a,double b,double x,double y)
{
    if(x<y)
    {
        return (scale(x,a,b)-scale(a,a,b))*(scale(b,a,b)-scale(y,a,b))/(scale(b,a,b)-scale(a,a,b));
    }
    else
    {
        return (scale(y,a,b)-scale(a,a,b))*(scale(b,a,b)-scale(x,a,b))/(scale(b,a,b)-scale(a,a,b));
    }
}

/* ************ Public functions ************ */

double Sticky::v0(double a, double z, double b)
{
//    cout << "Values in function v0:" << a <<" " <<z << " " <<b <<" "<< endl;
    if (!((a <= z) && (z <= b)))
    {
        cout << "Error when calling the function v0, a=" << a <<", x= "<< z <<",b = "<< b << " " << ". The expression a<x<b is thus: " << !((a < z) && (z < b)) << endl;
        return 0;
    }
    return (scale(z,a,b)-scale(a,a,b))/(scale(b,a,b)-scale(a,a,b));
}


double Sticky::v1_plus(double a, double x, double b)
{
//    cout << endl << "Values in function:" << a <<" " <<x << " " <<b <<" "<< endl;
    if (!((a <= x) && (x <= b)))
    {
        cout << "Error when calling the function v1_minus, a=" << a <<", x= "<< x <<",b = "<< b << " " << ". The expression a<x<b is thus: " << !((a < x) && (x < b)) << endl;
        return 0;
    }

    double sticky_term = 0;
    if (a<0 && b>0)
    {
        sticky_term = rho * green(a,b,x,0)  * v0(a,0.0,b);
    }

    return (x-a)*(b-x)   *   ((2/3)*pow(x-a,2)/pow(b-a,2)   +   (b-x)/(b-a)    -   (2/3)*pow(b-x,2)/pow(b-a,2) )   +    sticky_term  ;
}

double Sticky::v1_minus(double a, double x, double b)
{
//    cout << endl << "Values in function:" << a <<" " <<x << " " <<b <<" "<< endl;
    if (!((a <= x) && (x <= b)))
    {
        cout << "Error when calling the function v1_minus, a=" << a <<", x= "<< x <<",b = "<< b << " " << ". The expression a<x<b is thus: " << !((a < x) && (x < b)) << endl;
        return 0;
    }

    double sticky_term = 0;
    if (a<0 && b>0)
    {
        sticky_term = rho * green(a,b,x,0);
    }
    return   (x-a)*(b-x)   +    sticky_term     -   v1_plus(a,x,b);
}



