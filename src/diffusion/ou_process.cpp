#include "ou_process.h"
#include <math.h>
#include <iostream>
#include <cmath>
#include <gsl/gsl_sf_dawson.h>

using namespace std;


/* ************ Constructors ************ */



Ou_process::Ou_process()
{
    is_numerical_v0 = 1;
    is_numerical_v1 = 0;
    alpha = 1;
    mu = 0;
    sigma = 1;
    rho = 0;
}



Ou_process::Ou_process(double alpha0, double mu0, double sigma0, double rho0)
{
    is_numerical_v0 = 1;
    is_numerical_v1 = 0;
    alpha = alpha0;
    mu = mu0;
    sigma = sigma0;
    rho = rho0;
}



Ou_process::~Ou_process()
{

}


/* ************ Private functions ************ */


double erfi(double x)
{
		if (x > 0)
		{
			return exp(x*x) * gsl_sf_dawson(x);
		}
		else
		{
			return -exp(x*x) * gsl_sf_dawson(abs(x));
		}
}

double Ou_process::scale(double x, double a, double b)
{
	double c =  alpha / (sigma * sigma); 
	return erfi( sqrt(c) * ( x - mu));
}

double Ou_process::green(double a,double b,double x,double y)
{
    double max_xy = max(x,y);
    double min_xy = min(x,y);
    return (scale(min_xy,a,b) - scale(a,a,b))*(scale(b,a,b) - scale(max_xy,a,b))/(scale(b,a,b)-scale(a,a,b));
    //return v0(a,min_xy,b) * (1 - v0(a,max_xy,b));
}



/* ************ Public functions ************ */



double Ou_process::v0(double a, double x, double b)
{
    if (!(universal_functions::number_order_verification(a, x, b, "Error while calling function v0!"))) return 0;
    
	return (scale(x,a,b) - scale(a,a,b))/ (scale(b,a,b) - scale(a,a,b));
}


double Ou_process::dv0(double a, double z, double b)
{
    if (!(universal_functions::number_order_verification(a, z, b, "Error while calling function dv0!"))) return 0; 
    
    double c =   alpha / (sigma * sigma);
    return  sqrt(c) * exp( c * pow(z - mu,2)) ;
}


double Ou_process::dv1_plus(double a, double x, double y, double b)
{
	if (!(universal_functions::number_order_verification(a, y, b, "Error while calling function dv1_plus!"))) return 0;
	
	double dm = 2.0 / (pow(sigma,2) * dv0(a,y,b));
    return green(a,b,x,y) * v0(a,y,b) * dm;
}



double Ou_process::dv1_minus(double a, double x, double y, double b)
{
	if (!(universal_functions::number_order_verification(a, y, b, "Error while calling function dv1_minus!"))) return 0;
	    
	double dm = 2.0 / (pow(sigma,2) * dv0(a,y,b));
    return green(a,b,x,y) * (1 - v0(a,y,b)) * dm;
}


double Ou_process::v1_plus_numerical(double a, double x, double b, int N)
{
    double Ix = 0;
    double xi,xii;
    double sticky_term = 0;
    for (int i=0;i<N-1;i++)
    {
            xi = a + i*(b-a)/N;
            xii = a + (i+1)*(b-a)/N;
            Ix = Ix + ((dv1_minus(a,x,xi,b)+dv1_minus(a,x,xii,b))/2)*(b-a)/N;
    }
    if ((a < 0) && (b > 0))
    {
		//double dm = 2.0 / (pow(sigma,2) * dv0(a,0,b));
		sticky_term = green(a,b,x,0)* v0(a,0,b) * rho; //* dm;	
	}
    return Ix + sticky_term;
}

double Ou_process::v1_minus_numerical(double a, double x, double b, int N)
{
    double Ix = 0;
    double xi,xii;
    double sticky_term = 0;
    for (int i=0;i<N-1;i++)
    {
            xi = a + i*(b-a)/N;
            xii = a + (i+1)*(b-a)/N;
            Ix = Ix + ((dv1_plus(a,x,xi,b)+dv1_plus(a,x,xii,b))/2)*(b-a)/N;
    }
    if ((a < 0) && (b > 0))
    {
		//double dm = 2.0 / (pow(sigma,2) * dv0(a,0,b));
		sticky_term = green(a,b,x,0)*  (1 - v0(a,0,b)) * rho; // *dm;	
	}
    return Ix + sticky_term;
}

double Ou_process::v1_plus(double a, double x, double b){   return 0;}
double Ou_process::v1_minus(double a, double x, double b){   return 0;}














