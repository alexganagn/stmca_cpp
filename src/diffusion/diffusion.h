#ifndef DIFFUSION_H
#define DIFFUSION_H


class Diffusion
{
protected:
   // Diffusion();
    double scale(double x, double a, double b){return 0;}
    double green(double a,double b,double x,double y){return 0;}
public:
    double * v0_tab;
    double * v0prime_tab;
    bool is_numerical_v0;
    bool is_numerical_v1;

 //   virtual int fill_arrays(){return 0;};
    virtual double v0(double x, double a, double b){return 0;}
    virtual double v1_plus(double x, double a, double b){return 0;}
    virtual double v1_minus(double x, double a, double b){return 0;}
    virtual double dv0(double a, double z, double b){return 0;}
    virtual double dv1_plus(double a, double x,  double y,double b){return 0;}
    virtual double dv1_minus(double a, double x,  double y,double b){return 0;}
    virtual double v1_plus_numerical(double a, double x, double b, int N){return 0;}
    virtual double v1_minus_numerical(double a, double x, double b, int N){return 0;}
    virtual double sigma_volatility(double x){return 0;}
    virtual int fill_v0_tab(double a,double b){return 0;}
   // virtual ~Diffusion();
};

#endif // DIFFUSION_H
