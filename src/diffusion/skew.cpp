#include "skew.h"
#include <math.h>
#include <iostream>

using namespace std;


/* ************ Constructors ************ */

Skew::Skew()
{
    is_numerical_v0 = 1;
    is_numerical_v1 = 1;
    beta = 1./2;
}

Skew::Skew(double x)
{
    is_numerical_v0 = 1;
    is_numerical_v1 = 1;
    beta = x;
}

Skew::~Skew()
{

}


/* ************ Private functions ************ */

double Skew::scale(double x, double a, double b)
{
    if (x<0)
    {
        return x/(1-beta);
    }
    else
    {
        return x/beta;
    }
}

double Skew::green(double a,double b,double x,double y)
{
    if(x<y)
    {
        return (scale(x,a,b)-scale(a,a,b))*(scale(b,a,b)-scale(y,a,b))/(scale(b,a,b)-scale(a,a,b));
    }
    else
    {
        return (scale(y,a,b)-scale(a,a,b))*(scale(b,a,b)-scale(x,a,b))/(scale(b,a,b)-scale(a,a,b));
    }
}

/* ************ Public functions ************ */

double Skew::v0(double a, double z, double b)
{
//    cout << endl << "Values in function:" << a <<" " <<z << " " <<b <<" "<< endl;
    if (!((a <= z) && (z <= b)))
    {
        cout << "Error when calling the function v0, a=" << a <<", x= "<< z <<",b = "<< b << " " << ". The expression a<x<b is thus: " << !((a < z) && (z < b)) << endl;
        return 0;
    }
    return (scale(z,a,b)-scale(a,a,b))/(scale(b,a,b)-scale(a,a,b));
}


double Skew::v1_plus(double a, double x, double b)
{
//    cout << endl << "Values in function:" << a <<" " <<x << " " <<b <<" "<< endl;
    if (!((a <= x) && (x <= b)))
    {
        cout << "Error when calling the function v1_minus, a=" << a <<", x= "<< x <<",b = "<< b << " " << ". The expression a<x<b is thus: " << !((a < x) && (x < b)) << endl;
        return 0;
    }
    
    return  (x-a)*(b-x)   *   ((2/3)*pow(x-a,2)/pow(b-a,2)   +   (b-x)/(b-a)    -   (2/3)*pow(b-x,2)/pow(b-a,2) )  ;
    // NEW CMP
    if(a< 0 && b>0)
    {
    return -pow(b,2) * scale(a,a,b)/(scale(b,a,b)-scale(a,a,b))         -(scale(b,a,b)/pow(scale(b,a,b)-scale(a,a,b),2)) *(2/3) * (1/(1-beta)) * pow(a,3)          +(scale(a,a,b)/pow(scale(b,a,b)-scale(a,a,b),2)) *(2/3) * (1/beta) * pow(b,3) ;
    }
    else if(b<0)
    {
        return (1-beta) * ( (x-a)*(b-x)   *   ((2/3)*pow(x-a,2)/pow(b-a,2)   +   (b-x)/(b-a)    -   (2/3)*pow(b-x,2)/pow(b-a,2) ));
    }
    else if(a>0)
    {
        return beta * ( (x-a)*(b-x)   *   ((2/3)*pow(x-a,2)/pow(b-a,2)   +   (b-x)/(b-a)    -   (2/3)*pow(b-x,2)/pow(b-a,2)  ));
    }
    else
    {
      return 0;
    }

}

double Skew::v1_minus(double a, double x, double b)
{
//    cout << endl << "Values in function:" << a <<" " <<x << " " <<b <<" "<< endl;
    if (!((a <= x) && (x <= b)))
    {
        cout << "Error when calling the function v1_minus, a=" << a <<", x= "<< x <<",b = "<< b << " " << ". The expression a<x<b is thus: " << !((a < x) && (x < b)) << endl;
        return 0;
    }

    return  (x-a)*(b-x)      - v1_plus(a, x, b);
    // NEW CMP
    if(a< 0 && b>0)
    {
    return  pow(a,2) * scale(b,a,b)/(scale(b,a,b)-scale(a,a,b))   -pow(b,2) * scale(a,a,b)/(scale(b,a,b)-scale(a,a,b))    -     v1_plus(a, x, b);
    }
    else if(b<0)
    {
        return (1-beta) * (x-a)*(b-x)      - v1_plus(a, x, b);
    }
    else if(a>0)
    {
        return beta * (x-a)*(b-x)          - v1_plus(a, x, b);
    }
    else
    {
      return 0;
    }
}




