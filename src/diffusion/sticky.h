#include "diffusion.h"
#ifndef STICKY_H
#define STICKY_H

class Sticky : public Diffusion
{
private:
    double rho;
    double scale(double x, double a, double b);
    double green(double a,double b,double x,double y);
public:
    Sticky();
    Sticky(double x);
    ~Sticky();
    double v0(double x, double a, double b);
    double v1_plus(double x, double a, double b);
    double v1_minus(double x, double a, double b);
};

#endif // STICKY_H
