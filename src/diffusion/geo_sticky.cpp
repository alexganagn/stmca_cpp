#include "geo_sticky.h"
#include <math.h>
#include <iostream>
#include <cmath>


using namespace std;


/* ************ Constructors ************ */



Geo_sticky::Geo_sticky()
{
    is_numerical_v0 = 1;
    is_numerical_v1 = 0;
    sigma = 0.2;
    zeta = 100;
    rho = 1.0;
}



Geo_sticky::Geo_sticky( double mu0, double sigma0, double rho0, double zeta0)
{
    is_numerical_v0 = 1;
    is_numerical_v1 = 0;
    sigma = sigma0;
    zeta = zeta0;
    rho = rho0;

}



Geo_sticky::~Geo_sticky()
{

}


/* ************ Private functions ************ */



double Geo_sticky::scale(double x, double a, double b)
{
    return x;
}

double Geo_sticky::green(double a,double b,double x,double y)
{
    double max_xy = max(x,y);
    double min_xy = min(x,y);
    return (scale(min_xy,a,b)-scale(a,a,b))*(scale(b,a,b)-scale(max_xy,a,b))/(scale(b,a,b)-scale(a,a,b));
    //return v0(a,min_xy,b) * (1 - v0(a,max_xy,b));
}



/* ************ Public functions ************ */



double Geo_sticky::v0(double a, double z, double b)
{
    if (!((a <= z) && (z <= b)))
    {
        cout << "Error when calling the function v0, a=" << a <<", x= "<< z <<",b = "<< b << " " << ". The expression a<x<b is thus: " << !((a < z) && (z < b)) << endl;
        return 0;
    }


    return (scale(z,a,b)-scale(a,a,b))/(scale(b,a,b)-scale(a,a,b));

}


double Geo_sticky::v1_plus(double a, double x, double b)
{
    return 0;
}


double Geo_sticky::v1_minus(double a, double x, double b)
{
    return 0;
}

double Geo_sticky::dv0(double a, double z, double b)
{
    return 0;
}


double Geo_sticky::dv1_plus(double a, double x, double y, double b)
{
    double dm_dens=0;  
    dm_dens =  2 / (sigma*sigma * y*y);
    return green(a,b,x,y) * v0(a,y,b) * dm_dens;
}



double Geo_sticky::dv1_minus(double a, double x, double y, double b)
{
    double dm_dens = 0;
    dm_dens =  2 / (sigma*sigma * y*y);
    return green(a,b,x,y) * (1 - v0(a,y,b)) * dm_dens;
}

double Geo_sticky::v1_plus_numerical(double a, double x, double b, int N)
{
    double Ix = 0;
    double xi,xii;
    double sticky_term=0;
    
    for (int i=0;i<N-1;i++)
    {
            xi = a + i*(b-a)/N;
            xii = a + (i+1)*(b-a)/N;
            Ix = Ix + ((dv1_minus(a,x,xi,b)+dv1_minus(a,x,xii,b))/2)*(b-a)/N;
    }
    
    if (a<zeta & zeta<b)
    {
    	sticky_term = rho * green(a,b,x,zeta) * v0(a,zeta,b);
    	//cout << sticky_term << endl;
    }
    
    
    return abs(Ix) + sticky_term;
}

double Geo_sticky::v1_minus_numerical(double a, double x, double b, int N)
{
    double Ix = 0;
    double xi,xii;
    double sticky_term=0;
    
    for (int i=0;i<N-1;i++)
    {
            xi = a + i*(b-a)/N;
            xii = a + (i+1)*(b-a)/N;
            Ix = Ix + ((dv1_plus(a,x,xi,b)+dv1_plus(a,x,xii,b))/2)*(b-a)/N;
    }
    
    
    if (a<zeta & zeta<b)
    {
    	sticky_term = rho * green(a,b,x,zeta) * (1 - v0(a,zeta,b));
    	//cout << sticky_term << endl;
    }
    
    return abs(Ix) + sticky_term;
}











 
