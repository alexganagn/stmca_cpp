// Include the base Diffusion class
#include "diffusion.h"

// Header guard to prevent multiple inclusions
#ifndef CIR_PROCESS_H
#define CIR_PROCESS_H

// Class definition for Cir_process
class Cir_process : public Diffusion
{
private:
    // Private member variables for CIR parameters and normalization constant
    double alpha, mu, sigma, rho;
    double norm_const;

    // Private helper functions
    double scale(double x, double a, double b);
    double green(double a, double b, double x, double y);

public:
    // Public member variable to control the number of points for discretization
    int N_disc;

    // Constructors and Destructor
    Cir_process();
    Cir_process(double alpha0, double mu0, double sigma0, double rho0);
    ~Cir_process();

    // Diffusion model functions
    double v0(double x, double a, double b);
    double v0prime(double x, double a, double b);
    double v1_plus(double x, double a, double b);
    double v1_minus(double x, double a, double b);
    double dv0(double a, double z, double b);
    double dv1_plus(double a, double x, double y, double b);
    double dv1_minus(double a, double x, double y, double b);
    double v1_plus_numerical(double a, double x, double b, int N);
    double v1_minus_numerical(double a, double x, double b, int N);
    int fill_v0_tab(double a, double b);
};

#endif // CIR_PROCESS_H


