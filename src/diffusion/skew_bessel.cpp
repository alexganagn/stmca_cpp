#include "skew_bessel.h"
#include <math.h>
#include <iostream>
#include <cmath>


using namespace std;


/* ************ Constructors ************ */



Skew_bessel::Skew_bessel()
{
    is_numerical_v0 = 1;
    is_numerical_v1 = 0;
    delta = 1;
    beta = 1./2;
    rho = 0;
    nu = delta/2 - 1;
}



Skew_bessel::Skew_bessel(double delta0, double beta0, double rho0)
{
    is_numerical_v0 = 1;
    is_numerical_v1 = 0;
    delta = delta0;
    beta = beta0;
    rho = rho0;
    nu = delta/2 - 1;
}



Skew_bessel::~Skew_bessel()
{

}


/* ************ Private functions ************ */



double Skew_bessel::scale(double x, double a, double b)
{
    if (x>0)
    {
        //return pow(x,2-delta) * (beta)/(2 - delta);
        //return pow(x,-2*nu)/beta;
        return pow(x,-2*nu)/(-2*nu);
    }
    else 
    {
        //return -pow(abs(x),2-delta) * (1-beta)/(2 - delta);
        //return -pow(abs(x),-2*nu)/(1 - beta);
        return -pow(abs(x),-2*nu)/(-2*nu);
    }
}

double Skew_bessel::green(double a,double b,double x,double y)
{
    double max_xy = max(x,y);
    double min_xy = min(x,y);
    //return (scale(min_xy,a,b)-scale(a,a,b))*(scale(b,a,b)-scale(max_xy,a,b))/(scale(b,a,b)-scale(a,a,b));
    return v0(a,min_xy,b) * (1 - v0(a,max_xy,b));
}



/* ************ Public functions ************ */



double Skew_bessel::v0(double a, double z, double b)
{
    if (!((a <= z) && (z <= b)))
    {
        cout << "Error when calling the function v0, a=" << a <<", x= "<< z <<",b = "<< b << " " << ". The expression a<x<b is thus: " << !((a < z) && (z < b)) << endl;
        return 0;
    }

    
    if (a < 0 && b > 0)
    {
        if (z == 0)
        {
            if (delta > 2)// Theoretical result on the Skew bessel process, it never reacher 0 for delta > 2 thus we simulate a reflection...
            {
                return 1;
            }
            return beta;
        }
        // TO SPECIFY
        return (scale(z,a,b)-scale(a,a,b))/(scale(b,a,b)-scale(a,a,b));
    }
    else if (a == 0)
    {
        a = 0.000000001;
        return (scale(z,a,b)-scale(a,a,b))/(scale(b,a,b)-scale(a,a,b));
    }
    else if (b == 0)
    {
        b = -0.000000001;
        return (scale(z,a,b)-scale(a,a,b))/(scale(b,a,b)-scale(a,a,b));
    }
    else
    {
        return (scale(z,a,b)-scale(a,a,b))/(scale(b,a,b)-scale(a,a,b));
    }
}


double Skew_bessel::v1_plus(double a, double x, double b)
{
    return 0;
}


double Skew_bessel::v1_minus(double a, double x, double b)
{
    return 0;
}

double Skew_bessel::dv0(double a, double z, double b)
{
    return 0;
}


double Skew_bessel::dv1_plus(double a, double x, double y, double b)
{
    double dm_dens = 0;
    double a_t, b_t;
    if (a == 0)
    {
        a_t = 0.000000001;
    }
    else
    {
        a_t = a;
    } 
    if (b == 0)
    {
        b_t = -0.000000001;
    }
    else
    {
        b_t = b;
    }
    
    dm_dens = 2 * pow(abs(y),delta-1) * (scale(b, a, b) - scale(a, a, b));
    return green(a,b,x,y) * v0(a,y,b) * dm_dens ;
}



double Skew_bessel::dv1_minus(double a, double x, double y, double b)
{
    double dm_dens = 0;
    double a_t, b_t;
    if (a == 0)
    {
        a_t = 0.000000001;
    }
    else
    {
        a_t = a;
    } 
    if (b == 0)
    {
        b_t = -0.000000001;
    }
    else
    {
        b_t = b;
    }
    dm_dens = 2 * pow(abs(y),delta-1) * (scale(b_t, a_t, b_t) - scale(a_t, a_t, b_t));
    return green(a,b,x,y) * (1 - v0(a,y,b)) * dm_dens ;
}

double Skew_bessel::v1_plus_numerical(double a, double x, double b, int N)
{
    double Ix = 0;
    double xi,xii;
    for (int i=0;i<N-1;i++)
    {
            xi = a + i*(b-a)/N;
            xii = a + (i+1)*(b-a)/N;
            Ix = Ix + ((dv1_minus(a,x,xi,b)+dv1_minus(a,x,xii,b))/2)*(b-a)/N;
    }
    return abs(Ix);
    return 0;
}

double Skew_bessel::v1_minus_numerical(double a, double x, double b, int N)
{
    double Ix = 0;
    double xi,xii;
    for (int i=0;i<N-1;i++)
    {
            xi = a + i*(b-a)/N;
            xii = a + (i+1)*(b-a)/N;
            Ix = Ix + ((dv1_plus(a,x,xi,b)+dv1_plus(a,x,xii,b))/2)*(b-a)/N;
    }
    return abs(Ix);
    return 0;
}











 
