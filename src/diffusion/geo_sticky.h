#include "diffusion.h"
#ifndef GEO_STICKY_H
#define GEO_STICKY_H


class Geo_sticky : public Diffusion
{
private:
    double sigma,rho,zeta;
    double scale(double x, double a, double b);
    double green(double a,double b,double x,double y);

public:
    Geo_sticky();
    Geo_sticky(double mu0, double sigma0, double rho0, double zeta0);
    ~Geo_sticky();

    double v0(double x, double a, double b);
    double v1_plus(double x, double a, double b);
    double v1_minus(double x, double a, double b);
    double dv0(double a, double z, double b);
    double dv1_plus(double a, double x, double y, double b);
    double dv1_minus(double a, double x, double y, double b);
    double v1_plus_numerical(double a, double x, double b, int N);
    double v1_minus_numerical(double a, double x, double b, int N);
};

#endif // GEO_STICKY_H 
