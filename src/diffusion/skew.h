#include "diffusion.h"
#ifndef SKEW_H
#define SKEW_H


class Skew : public Diffusion
{
private:
    double beta;
    double scale(double x, double a, double b);
    double green(double a,double b,double x,double y);
public:

    Skew();
    Skew(double x);
    ~Skew();
   // int fill_arrays();
    double v0(double x, double a, double b);
    double v1_plus(double x, double a, double b);
    double v1_minus(double x, double a, double b);
};

#endif // SKEW_H
