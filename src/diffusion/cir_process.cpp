#include "cir_process.h"
#include <math.h>
#include <iostream>
#include <cmath>
#include <gsl/gsl_sf_dawson.h>

using namespace std;

/* ************ Constructors ************ */

Cir_process::Cir_process()
{
    // Default constructor
    is_numerical_v0 = 0;
    is_numerical_v1 = 0;
    alpha = 1;
    mu = 0;
    sigma = 1;
    rho = 0;
    v0_tab = (double *) malloc(sizeof(double) * 100);
    norm_const = 1;
    N_disc = 100;
}

Cir_process::Cir_process(double alpha0, double mu0, double sigma0, double rho0)
{
    // Constructor with parameters
    is_numerical_v0 = 0;
    is_numerical_v1 = 0;
    alpha = alpha0;
    mu = mu0;
    sigma = sigma0;
    rho = rho0;
    N_disc = 100;
    v0_tab = (double *) malloc(sizeof(double) * N_disc);       // contains tabulated value of v0
    v0prime_tab = (double *) malloc(sizeof(double) * N_disc);  // contains tabulated values of the derivatives
    norm_const = 1;
}

Cir_process::~Cir_process()
{
    // Destructor
}

/* ************ Private functions ************ */

double Cir_process::scale(double x, double a, double b)
{
    // Implementation for the scale function
    return 0;
}

double Cir_process::green(double a, double b, double x, double y)
{
    // Implementation for the green function
    double max_xy = fmax(x, y);
    double min_xy = fmin(x, y);
    return v0(a, min_xy, b) * (1 - v0(a, max_xy, b));
}




/* ************ Public functions ************ */


// Return the value of v0 at z using linear interpolation
double Cir_process::v0(double a, double z, double b)
{
    int xup = int(fmin(int(floor(N_disc * (z - a) / (b - a))), N_disc));
    int xdown = int(fmax(int(floor(N_disc * (z - a) / (b - a))) - 1, 0));
    return 0.5 * (v0_tab[xup] + v0_tab[xdown]);
}

// Return the derivative of v0 at z using linear interpolation
double Cir_process::v0prime(double a, double z, double b)
{
    int xup = int(fmin(int(floor(N_disc * (z - a) / (b - a))), N_disc));
    int xdown = int(fmax(int(floor(N_disc * (z - a) / (b - a))) - 1, 0));
    return 0.5 * (v0prime_tab[xup] + v0prime_tab[xdown]);
}

// Derivative of the scale function, up to a multiplicative constant
double Cir_process::dv0(double a, double z, double b)
{
    double c = 2 * alpha / (sigma * sigma);
    return pow(a / z, mu * c) * exp(c * (z - a));
}

// Fill the tabulated values for computing the integrals using the mid-point method
int Cir_process::fill_v0_tab(double a, double b)
{
    double Ix = 0;
    double xi, xii;
    double step = (b - a) / double(N_disc);
    v0_tab[0] = 0.0; // The left end-point starts at 0.
    v0prime_tab[0] = dv0(a, a, b); // Left end-point
    for (int i = 1; i < N_disc; i++)
    {
        xi = a + i * step;
        xii = xi + step;
        v0prime_tab[i] = 0.5 * (dv0(a, xi, b) + dv0(a, xii, b));
        Ix = Ix + v0prime_tab[i] * step;
        v0_tab[i] = Ix;
    }
    // v0prime_tab[99] = dv0(a,b,b);
    // v0_tab[99] = 1;
    // Computation of the renormalization of v0 and its derivative
    // Ix contains the last point
    for (int i = 0; i < N_disc; i++)
    {
        v0_tab[i] = v0_tab[i] / Ix;
        v0prime_tab[i] = v0prime_tab[i] / Ix;
    }
    return 0;
}


// Returns the value of v1_plus at the given points a, x, and b (currently set to 0)
double Cir_process::v1_plus(double a, double x, double b)
{
    return 0;
}

// Returns the value of v1_minus at the given points a, x, and b (currently set to 0)
double Cir_process::v1_minus(double a, double x, double b)
{
    return 0;
}

// Calculates the derivative of v1_plus with respect to y using Green's function
double Cir_process::dv1_plus(double a, double x, double y, double b)
{
    return green(a, b, x, y) * 2.0 * v0(a, y, b) / (v0prime(a, y, b) * y * pow(sigma, 2));
}

// Calculates the derivative of v1_minus with respect to y using Green's function
double Cir_process::dv1_minus(double a, double x, double y, double b)
{
    return green(a, b, x, y) * (1 - v0(a, y, b)) * 2.0 / (v0prime(a, y, b) * y * pow(sigma, 2));
}

// Numerically computes the integral of v1_plus over the interval [a, b] using the mid-point method
double Cir_process::v1_plus_numerical(double a, double x, double b, int N)
{
    double Ix = 0;
    double xi, xii;
    double step = (b - a) / double(N_disc);
    for (int i = 0; i < N_disc - 1; i++)
    {
        xi = a + i * step;
        xii = xi + step;
        Ix = Ix + 0.5 * (dv1_plus(a, x, xi, b) + dv1_plus(a, x, xii, b)) * step;
    }
    return Ix;
}

// Numerically computes the integral of v1_minus over the interval [a, b] using the mid-point method
double Cir_process::v1_minus_numerical(double a, double x, double b, int N)
{
    double Ix = 0;
    double xi, xii;
    double step = (b - a) / double(N_disc);
    for (int i = 0; i < N_disc - 1; i++)
    {
        xi = a + i * step;
        xii = xi + step;
        Ix = Ix + 0.5 * (dv1_minus(a, x, xi, b) + dv1_minus(a, x, xii, b)) * step;
    }
    return Ix;
}















