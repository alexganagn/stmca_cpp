#include <iostream>
#include <fstream>
#include <cmath>
#include "trajsim_adv.h"

using namespace std;


Trajsim_adv::Trajsim_adv()
/*
Default constructor
*/
{
    T_horiz = 1.0;
    x0 = 0.0;
}


Trajsim_adv::Trajsim_adv(double time_horizon, double starting_position, Diffusion_tabular tabbed_diffusion)
/*
Custom constructor
*/
{
    T_horiz = time_horizon;
    x0 = starting_position;
    diff = tabbed_diffusion;

    max_Num_steps = tabbed_diffusion.grid0.N * tabbed_diffusion.grid0.N/2 ;
    traj_time = (double *) malloc(max_Num_steps * sizeof(double));
    traj_space = (double *) malloc(max_Num_steps * sizeof(double));
    
    terminal_values = (double *) malloc(100000 * sizeof(double));
    
    index_starting_point_ongrid();
}

Trajsim_adv::Trajsim_adv(double time_horizon, double starting_position, Diffusion_tabular tabbed_diffusion, Diffusion_tabular tabbed_diffusion2)
/*
Custom constructor
*/
{
    T_horiz = time_horizon;
    x0 = starting_position;
    diff = tabbed_diffusion;
    diff2 = tabbed_diffusion2;

    max_Num_steps = tabbed_diffusion.grid0.N * tabbed_diffusion.grid0.N/2 ;
    traj_time = (double *) malloc(max_Num_steps * sizeof(double));
    traj_space = (double *) malloc(max_Num_steps * sizeof(double));
    traj_time2 = (double *) malloc(max_Num_steps * sizeof(double));
    traj_space2 = (double *) malloc(max_Num_steps * sizeof(double));
    
    terminal_values = (double *) malloc(100000 * sizeof(double));
    
    index_starting_point_ongrid();
}


Trajsim_adv::~Trajsim_adv() {
   // Deallocate the memory that was previously reserved
   //  for this string.
   delete[] terminal_values;

   delete[] traj_time;
   delete[] traj_space;
   
   delete[] traj_time2;
   delete[] traj_space2;
}


int Trajsim_adv::index_starting_point_ongrid()
/*
Finding the starting point on the grid
*/
{
    int n=0;
    double tmp = abs(diff.grid0.p_arr[0]-x0);
    for(int i=0;i<diff.grid0.N;i++)
    {
        if(abs(diff.grid0.p_arr[i]-x0)<tmp)
        {
            n = i;
            tmp = abs(diff.grid0.p_arr[i]-x0);
        }
    }
    n0 = n;
    cout << "Starting point on grid: " << diff.grid0.p_arr[n0] << endl << endl;
    return 0;
}

int Trajsim_adv::reset_x0(double x)
/*
Reset startint point of diffusion
*/
{
    x0 = x;
    index_starting_point_ongrid();
    
    return 0;
}

int Trajsim_adv::reset_time(double T0)
/*
Reset time horizon for the simulations
*/
{
    T_horiz = T0;

    return 0;
}

/* *** TRAJECTORY SIMULATION *** */




int Trajsim_adv::single_traj_sim()
/*
	Simulation of a single trajectory using the STMCA Algorithm
*/
{
        double t0 = 0;
        int n=n0;
        double u;



        traj_time[0] = t0;
        traj_space[0] = diff.grid0.p_arr[n];

        int i=1;
        while (t0 < T_horiz)
        {
			
            u = dis(gen);
            if (u< diff.pplus[n])
            {
                t0 = t0 + diff.Tplus[n];
                n = n+1;
            }
            else
            {
               t0 = t0 + diff.Tminus[n];
               n = n-1;
            }
            if(i< max_Num_steps)
            {
                traj_time[i] = t0;
                traj_space[i] = diff.grid0.p_arr[n];
                //cout << traj_time[i-1] << " " << traj_space[i-1] << endl;
                i++;
            }
        }
        TV = traj_space[i-1];
        Num_steps = i;
        //cout << Num_steps << " " << max_Num_steps << endl;
    return 0;
}


int Trajsim_adv::single_traj_sim_cross()
/*
	Simulation of a single trajectory using the STMCA Algorithm
*/
{
        double t0=0;
        double t0_2=0;
        int n=n0;
        double u;



        traj_time[0] = t0;
        traj_space[0] = diff.grid0.p_arr[n];
        
        traj_time2[0] = t0_2;
        traj_space2[0] = diff.grid0.p_arr[n];

        int i=1;
        while (t0 < T_horiz || t0_2 < T_horiz)
        {
			
            u = dis(gen);
            if (u< diff.pplus[n])
            {
                t0 = t0 + diff.Tplus[n];
                n = n+1;
            }
            else
            {
               t0 = t0 + diff.Tminus[n];
               n = n-1;
            }
            
            if (u< diff2.pplus[n])
            {
                t0_2 = t0_2 + diff2.Tplus[n];
                n = n+1;
            }
            else
            {
               t0_2 = t0_2 + diff2.Tminus[n];
               n = n-1;
            }

            if(i< max_Num_steps)
            {
                traj_time[i] = t0;
                traj_space[i] = diff.grid0.p_arr[n];
                traj_time2[i] = t0_2;
                traj_space2[i] = diff2.grid0.p_arr[n];              
                //cout << traj_time[i-1] << " " << traj_space[i-1] << endl;
                i++;
            }
        }
        TV = traj_space[i-1];
        Num_steps = i;
        //cout << Num_steps << " " << max_Num_steps << endl;
    return 0;
}


int Trajsim_adv::single_traj_export_full(string filename)
{
    ofstream myfile;
    myfile.open(filename, ofstream::out | ofstream::trunc);
    if (myfile.is_open())
    {
        single_traj_sim();

        for(int i=1;i<Num_steps;i++)
        {
            myfile << traj_time[i-1] << ";" << traj_space[i-1]  << "\n";
            universal_functions::activity_indicator(i, Num_steps, "EXPORTING TRAJECTORY TO FILE: ");
        }
        universal_functions::completed_activity("EXPORTING TRAJECTORY TO FILE: ");
        myfile.close();
        cout << "TRAJECTORY EXPORTED TO: " << filename << endl << endl;
    }
    else cout << "Unable to open file"<< endl;


    return 0;
}


int Trajsim_adv::single_traj_export_full_cross(string filename, string filename2)
{
    ofstream myfile,myfile2;
    myfile.open(filename, ofstream::out | ofstream::trunc);
    myfile2.open(filename2, ofstream::out | ofstream::trunc);
    
    single_traj_sim_cross();
    
    if (myfile.is_open())
    {
        for(int i=1;i<Num_steps;i++)
        {
            myfile << traj_time[i-1] << ";" << traj_space[i-1]  << "\n";
            universal_functions::activity_indicator(i, Num_steps, "EXPORTING TRAJECTORY TO FILE: ");
        }
        universal_functions::completed_activity("EXPORTING TRAJECTORY TO FILE: ");
        myfile.close();
        cout << "TRAJECTORY EXPORTED TO: " << filename << endl << endl;
    }
    else cout << "Unable to open file"<< endl;
    
    if (myfile2.is_open())
    {
        for(int i=1;i<Num_steps;i++)
        {
            myfile2 << traj_time2[i-1] << ";" << traj_space2[i-1]  << "\n";
            universal_functions::activity_indicator(i, Num_steps, "EXPORTING TRAJECTORY TO FILE: ");
        }
        universal_functions::completed_activity("EXPORTING TRAJECTORY TO FILE: ");
        myfile2.close();
        cout << "TRAJECTORY EXPORTED TO: " << filename2 << endl << endl;
    }
    else cout << "Unable to open file"<< endl;

    return 0;
}


int Trajsim_adv::simulate_terminal_values(int MC_size)
/*
	Simulation Monte Carlo
*/
{
	for (int i = 0; i < MC_size; i++)
	{
		single_traj_sim();
		//cout << TV << endl;
		terminal_values[i] = TV;
		universal_functions::activity_indicator(i, MC_size, "SIMULATING TRAJECTORIES: ");
	}
	
	return 0;
}


int Trajsim_adv::export_TV(int MC_size,string filename)
/*
Finding the starting point on the grid
*/
{
    std::ofstream outfile(filename);  // create a file named "output.txt" for writing
    if (outfile.is_open()) {
        for (int i = 0; i < MC_size; i++){
            outfile << terminal_values[i] << endl;  // write each element followed by a space
        }
        outfile.close();  // close the file
        std::cout << "List exported to file successfully.\n";
    }
    else {
        std::cout << "Unable to open file.\n";
    }
    return 0;
}


double Trajsim_adv::max_path()
{
    double out=traj_space[0];
    for(int i=1;i<Num_steps-1;i++)
    {
        if (traj_space[i]>out)
        {
            out=traj_space[i];
        }
    }
    return out;
}


double Trajsim_adv::min_path()
{
    double out=traj_space[0];
    for(int i=1;i<Num_steps-1;i++)
    {
        if (traj_space[i]<out)
        {
            out=traj_space[i];
        }
    }
    return out;
}


double Trajsim_adv::payoff_function(double x)
{
    return x-10;
}


int Trajsim_adv::MC_sim(int MC_size)
/*
	Simulation Monte Carlo
*/
{
	double MC_exp = 0;
	double MC_var = 0;
	for (int i = 0; i < MC_size; i++)
	{
		MC_exp+=terminal_values[i]/MC_size;
	}

    for (int i = 0; i < MC_size; i++)
	{
		MC_var+=pow(terminal_values[i]-MC_exp,2)/(MC_size-1);
	}
	
	cout << "Monte Carlo estimation" << MC_exp;
	cout << "Monte Carlo variance" << MC_var;
	
	return 0;
}

