#ifndef TRAJSIM_ADV_H
#define TRAJSIM_ADV_H

#include<algorithm>
#include "../diffusion/diffusion.h"
#include "../diffusion_tabular/diffusion_tabular.h"
#include "../helper_functions/universal_functions.h"
#include "../grid/grid.h"
//#include "local_time_statistics.h"
#include <random>


static std::random_device rd;  //Will be used to obtain a seed for the random number engine
static std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
static std::uniform_real_distribution<> dis(0.0, 1.0);


using namespace std;

//static std::random_device rd;  //Will be used to obtain a seed for the random number engine
//static std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
//static std::uniform_real_distribution<> dis(0.0, 1.0);


class Trajsim_adv
{
public:
	double sigmam,sigmap;
	double threshold;
    double T_horiz; // time horizon, starting point on grid
    int n0; // index of initial poisition on the grid
    double TV;
    Diffusion_tabular diff,diff2;

    Trajsim_adv();
    Trajsim_adv(double time_horizon, double starting_position, Diffusion_tabular tabbed_diffusion);
    Trajsim_adv(double time_horizon, double starting_position, Diffusion_tabular tabbed_diffusion, Diffusion_tabular tabbed_diffusion2);
    ~Trajsim_adv();
    
    int reset_x0(double x);
    int reset_time(double T0);

    int generate_uniformized_in_time_version_of_a_trajectory(int N_steps);
    int single_traj_sim();
    int single_traj_sim_cross();
    int index_starting_point_ongrid();
    int single_traj_export_full(string filename);
    int single_traj_export_full_cross(string filename,string filename2);
    int single_traj_export_unif(string filename, int N);

    double max_path();
    double min_path();

    int lt_approx_N_alpha(double * arr_N_steps, double * arr_alphas, string filename);
    int export_lt_estimator_to_file(string filename, int N, double* arr1, double* arr2, double* arr3 , double* arr4);

    double payoff_function(double x);
	int MC_sim(int MC_size);
	int simulate_terminal_values(int MC_size);
	int export_TV(int MC_size,string filename);

private:
	double x0;
    double * traj_time;
    double * traj_space;
    double * traj_time2;
    double * traj_space2;
    double * unif_space;
    double * terminal_values;
    double h_timestep;
    int Num_steps;
    int max_Num_steps;
};

#endif // TRAJSIM_ADV_H


