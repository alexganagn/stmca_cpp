#include "universal_functions.h"
#include <fstream>

universal_functions::universal_functions()
{
    // Constructor
}

universal_functions::~universal_functions()
{
    // Destructor
}

// Displays an activity indicator based on the progress i out of N with a given message
int universal_functions::activity_indicator(int i, int N, string message)
{
    // ANSI color codes for console text formatting
    string activity_symbol[6];
    string completion_symbol;

    // Different symbols for the activity indicator and completion symbol
    activity_symbol[0] = "(o     )";
    activity_symbol[1] = "( o    )";
    activity_symbol[2] = "(  o   )";
    activity_symbol[3] = "(   o  )";
    activity_symbol[4] = "(    o )";
    activity_symbol[5] = "(     o)";
    completion_symbol = "(oooooo)";

    // Display activity indicator and progress percentage
    cout << message << "\x1B[94m" << activity_symbol[i % 6] << "\033[0m" << " PROGRESS: " << floor(100 * float(i) / N) << "% ";
    cout << "\r";  // Move the cursor to the beginning of the line
    cout.flush();   // Flush the output stream

    return 0;
}

// Displays a completion message for a given activity
int universal_functions::completed_activity(string message)
{
    cout << message << "\x1B[94m" << "COMPLETED\033[0m                               " << endl;
    return 0;
}

// Verifies if the numbers x, y, z are in ascending order
bool universal_functions::number_order_verification(double x, double y, double z, string message)
{
    bool out = ((x <= y) && (y <= z));
    if (!out)
    {
        cout << std::boolalpha;
        cout << message << " | The relation: " << x << " < " << y << " < " << z << " is " << out << endl;
    }
    return out;
}

// Placeholder for writing a 1D array to a file
int universal_functions::write_1D_array_to_file(string filename, double *arr, int array_size)
{
    // TODO: Implement the functionality to write a 1D array to a file
    return 0;
}

// Placeholder for writing a 2D array to a file
int universal_functions::write_2D_array_to_file(string filename, double **p_arr, int array_size_c, int array_size_r)
{
    // TODO: Implement the functionality to write a 2D array to a file
    return 0;
}
