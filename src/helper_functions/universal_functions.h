#ifndef UNIVERSAL_FUNCTIONS_H
#define UNIVERSAL_FUNCTIONS_H

#include <iostream>
#include <cmath>

using namespace std;

// ANSI color codes for console text formatting
static string yellow = "\x1B[93m";
static string cyan = "\x1B[37m";
static string green = "\x1B[92m";
static string end_color = "\033[0m";

class universal_functions
{
public:
    // Constructors and Destructor
    universal_functions();
    virtual ~universal_functions();

    // Static Methods

    // Displays an activity indicator based on the progress i out of N with a given message
    static int activity_indicator(int i, int N, string message);

    // Displays a completion message for a given activity
    static int completed_activity(string message);

    // Verifies if the numbers x, y, z are in ascending order
    static bool number_order_verification(double x, double y, double z, string message);

    // Writes a 1D array to a file with the specified filename
    static int write_1D_array_to_file(string filename, double *arr, int array_size);

    // Writes a 2D array to a file with the specified filename
    static int write_2D_array_to_file(string filename, double **p_arr, int array_size_c, int array_size_r);

protected:
private:
};

#endif // UNIVERSAL_FUNCTIONS_H

